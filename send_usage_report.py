from argparse import ArgumentParser
import os
import json
import time

import django

# pylint: disable=wrong-import-position
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hydra.settings")
django.setup()

from django.conf import settings
import requests

from sidekick.views.views import get_report_rows


def main():
    ap = ArgumentParser()
    ap.add_argument('-b', '--botname', dest='botname', required=True, help='Name of the bot')
    ap.add_argument(
        '-d', '--debug', dest='debug', default=False, action='store_true', required=False,
        help='In this mode, the post JSON will be written to stdio, not calling the API endpoint'
        )

    options = ap.parse_args()

    rows = get_report_rows(options.botname, needs_iso_date=True)

    rows.sort(key=lambda x: x['User'])

    if options.debug:
        print(json.dumps(rows))
    else:
        headers = {'token': settings.REPORT_API_TOKEN, 'Content-type': 'application/json'}
        requests.post(settings.SAP_API_ENDPOINT, json=rows, headers=headers)
        # async with ClientSession(headers=headers) as session:
        #     async with session.post(settings.REPORT_API_ENDPOINT, json=rows) as response:
        #         if response.status != 200:
        #             print(f"API call failed with status {response.status}: {response.reason}")
        #         else:
        #             print(f"API call succeeded with status {response.status}: {response.content}")


if __name__ == '__main__':
    st = time.time()
    main()
